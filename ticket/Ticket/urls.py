from django.urls import path
from .import views


app_name='Ticket'

urlpatterns = [
    path('<int:user_id>', views.tickets ,name='tickets'),
    path('add_ticket/<int:user_id>/',views.add_ticket,name='add_ticket'),
    path('<int:year>-<int:month>-<int:day>/<slug:slug>/',views.detail_ticket,name='detail_ticket'),
    path('add_reply/<int:tickets_id>/<int:comment_id>',views.add_reply,name='add_reply'),
    
]