
from django.contrib.messages.api import success
from django.shortcuts import get_object_or_404, redirect, render
from .models import ticket,Comment
from .forms import AddCommentForm, AddReplyForm, AddTicketForm
from django.contrib import messages
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin

def tickets(request,user_id):
    ticket_item= ticket.objects.filter(user_id=user_id)
    return render (request,'ticket/ticket.html',{'ticket_item':ticket_item})


def detail_ticket(request,year,month,day,slug):
    tickets=get_object_or_404(ticket,date__year=year,date__month=month,date__day=day,slug=slug)
    comments=Comment.objects.filter(ticket=tickets ,is_reply=False)
    reply_form = AddReplyForm()
    if request.method == 'POST':
        form=AddCommentForm(request.POST)
        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.ticket = tickets
            new_comment.user = request.user
            new_comment.save()
            messages.success(request,'your comment submitted',success)
            form=AddCommentForm()
    else:
        form=AddCommentForm()
    return render (request,'ticket/detail_ticket.html',{'tickets':tickets, 'comments':comments ,'form':form, 'reply':reply_form})


def add_ticket(request,user_id):
    if request.method == 'POST':
        form = AddTicketForm(request.POST)
        if form.is_valid():
            new_ticket = form.save(commit = False)
            new_ticket.user = request.user
            new_ticket.slug = slugify(form.cleaned_data['title'][:3])
            new_ticket.save()
            return redirect("account:dashboard",user_id)
    else:
        form = AddTicketForm()
    return render(request,'ticket/add_ticket.html',{'form':form})


def add_reply(request,tickets_id,comment_id):
    tickets=get_object_or_404(ticket,id=tickets_id)
    comment=get_object_or_404(Comment,pk=comment_id)
    if request.method == 'POST':
        form = AddReplyForm(request.POST)
        if form.is_valid():
            reply=form.save(commit=False)
            reply.user = request.user
            reply.ticket = tickets
            reply.reply = comment
            reply.is_reply = True
            reply.save()
            messages.success(request,'your reply submitted',"success" )
        return redirect('Ticket:detail_ticket',tickets.date.year,tickets.date.month,tickets.date.day,tickets.slug)
