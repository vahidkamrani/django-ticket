from django import forms
from django.db.models import fields
from django.forms import widgets
from.models import ticket,Comment

class AddTicketForm(forms.ModelForm):
    class Meta:
        model = ticket
        exclude = ("user","slug")
        
        
class AddCommentForm(forms.ModelForm):
    class Meta:
        model=Comment
        fields=('body',)
        widgets={
            'body' : forms.Textarea(attrs={"class":"form-control"})
        }

class AddReplyForm(forms.ModelForm):
    class Meta:
        model=Comment
        fields=('body',)
        widgets={
            'body' : forms.Textarea(attrs={"class":"form-control"})
        }