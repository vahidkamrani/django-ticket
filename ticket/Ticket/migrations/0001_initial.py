# Generated by Django 3.2.8 on 2021-11-06 11:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ticket',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('ticketID', models.IntegerField()),
                ('slug', models.SlugField()),
                ('status', models.TextField(max_length=500)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('receiverID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='receiverID', to=settings.AUTH_USER_MODEL)),
                ('userID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='UserID', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
