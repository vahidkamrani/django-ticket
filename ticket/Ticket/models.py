from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE
from django.urls import reverse

class ticket(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,related_name='userr' )
    title=models.CharField(max_length=20)
    ticketID=models.IntegerField()
    slug = models.SlugField(max_length = 50)
    status=models.TextField(max_length=500)
    date=models.DateTimeField(auto_now_add=True)
    receiverID=models.ForeignKey(User,on_delete=models.CASCADE)
    
    def __str__(self):
        return f'{self.user} - {self.status[:30]}'
    
    def get_absolute_url(self):
        return reverse('Ticket:detail_ticket', args=[self.date.year, self.date.month, self.date.day, self.slug])
    
    
    
class Comment(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,related_name='ucomment')
    ticket=models.ForeignKey(ticket,on_delete=models.CASCADE,related_name='tcomment')
    reply=models.ForeignKey('self',on_delete=models.CASCADE,null=True,blank=True,related_name='rcomment')
    is_reply=models.BooleanField(default=False)
    body=models.TextField(max_length=300)
    created=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f'{self.user} - {self.body[:30]}'
    class Meta:
        ordering = ('created',)
    
    