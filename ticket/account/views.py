
from django.shortcuts import redirect, render,get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from .forms import UserLoginForm,UserRegistrationForm,ProfileForm
from django.contrib import messages
from Ticket.models import ticket



def user_login(request):
    if request.method == 'POST':
        form=UserLoginForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            user=authenticate(request,username=cd['username'],password=cd['password'])
            if user is not None:
                login(request,user)
                messages.success(request,'you logged successfully','success')
                return redirect ('account:dashboard',user.id)
            else:
                messages.error(request,'wrong username or password','warning')
    else:
        form=UserLoginForm()        
    return render (request,'account/login.html',{'form':form})


def user_register(request):
    if request.method == 'POST':
        form=UserRegistrationForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            user = User.objects.create_user(cd['username'],cd['email'],cd['password'])
            login(request,user)
            messages.success(request,'you Registered successfully','success')
            return redirect('account:profile',user.id)
    else:
        form=UserRegistrationForm()
    return render (request,'account/register.html',{'form':form})


def user_logout(request):
    logout(request)
    messages.success(request,'you loggedout successfully','success')
    return redirect ('account:login')


def user_dashboard(request,user_id):
    receiverID_id=user_id
    user=get_object_or_404(User,id=user_id)
    tickets=ticket.objects.filter(user = user,)
    receiverID=get_object_or_404(User,id=receiverID_id)
    ticketss=ticket.objects.filter(receiverID = receiverID)
    return render(request,'account/dashboard.html',{'user':user , 'tickets':tickets , 'ticketss':ticketss})


def user_receiverID (request,receiverID_id):
    receiverID=get_object_or_404(User,id=receiverID_id)
    ticketss=ticket.objects.filter(receiverID = receiverID)
    return render(request,'account/dashboard.html',{'receiverID':receiverID , 'ticketss':ticketss})
    
    
    


def user_profile(request,user_id):
    if request.method == "POST":
        form = ProfileForm( data = request.POST)
        if form.is_valid():
            form.save()  
            return redirect('account:login')
        else:
            return render(request,'account/profile.html',{'form':form})
    form = ProfileForm()
    return render(request,'account/profile.html',{'form':form})