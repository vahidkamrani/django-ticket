# Generated by Django 3.2.8 on 2021-11-08 08:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstname', models.CharField(max_length=25)),
                ('lastname', models.CharField(max_length=25)),
                ('userID', models.CharField(max_length=10)),
                ('phonenumber', models.CharField(max_length=11)),
                ('postID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='account.post')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='userlogin',
        ),
        migrations.DeleteModel(
            name='userregistration',
        ),
    ]
