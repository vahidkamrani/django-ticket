# Generated by Django 3.2.8 on 2021-11-08 10:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20211108_0826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='postID',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='account.post'),
        ),
    ]
