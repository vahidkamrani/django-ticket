from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE
from django.db.models.signals import post_save


class post(models.Model):
    postID=models.IntegerField()
    name=models.CharField(max_length=25)
    
    def __str__(self):
        return f'{self.postID} - {self.name}'
    
    
class Profile(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE) 
    firstname=models.CharField(max_length=25) 
    lastname=models.CharField(max_length=25)
    userID=models.CharField(max_length=10)
    postID=models.ForeignKey(post, on_delete=models.CASCADE,blank=True,null=True)
    phonenumber=models.CharField(max_length=11)
        
    def __str__(self):
        return self.user.username



def save_profile(sender,**kwargs):
    if kwargs['created']:
        p1=Profile(user = kwargs['instance'])
        p1.save()
    
post_save.connect(save_profile,sender=User)




