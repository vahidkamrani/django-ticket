from django import forms
from django.db.models import fields
from .models import *


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields='__all__'


class UserLoginForm(forms.Form):
    username=forms.CharField(max_length=25, widget=forms.TextInput(
                                attrs={"class":"form-control" , 'placeholder':'Username'}))
    password=forms.CharField(max_length=25, widget=forms.PasswordInput(
                                attrs={'class':'form-control' , 'placeholder':'Password'}))
    


class UserRegistrationForm(forms.Form):
    username=forms.CharField(max_length=25, widget=forms.TextInput(
                                attrs={"class":"form-control" , 'placeholder':'Username'}))
    email=forms.EmailField(max_length=50,widget=forms.EmailInput(
                                attrs={'class':'form-control' , 'placeholder':'Emailaddress' }))
    password=forms.CharField(max_length=25, widget=forms.PasswordInput(
                                attrs={'class':'form-control' , 'placeholder':'Password'}))
    
    